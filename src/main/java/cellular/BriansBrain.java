package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
    
	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a BriansBrain cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i<currentGeneration.numRows();i++){
			for(int j = 0; j<currentGeneration.numColumns();j++){
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int counter = countNeighbors(row, col, CellState.ALIVE); 
		if (getCellState(row, col) == CellState.ALIVE){
			return CellState.DYING;
			}
		if (getCellState(row, col) == CellState.DYING){
			return CellState.DEAD;
		}
        if ((getCellState(row, col) == CellState.DEAD)&&counter ==2){ 
            return CellState.ALIVE;
        }
		return CellState.DEAD;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int counter = 0;
		for (int r = row-1; r<=row+1;r++){
			for (int c = col-1; c<=col+1;c++){
				if ((row == r && col == c)||r > numberOfRows()-1||r<0||c > numberOfColumns()-1||c<0){
					continue;
				}
				if (getCellState(r, c)==state){
					counter++;
				}
			}
		}
		return counter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}

