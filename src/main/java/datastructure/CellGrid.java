package datastructure;

import cellular.CellState;

import java.awt.Color;
import java.util.ArrayList;

public class CellGrid implements IGrid {
    int cols;
    int rows;
    ArrayList<CellState> CellState;



    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        cols = columns;
        CellState = new ArrayList<CellState>();
        for (int row = 0; row<rows; row++){
            for (int col = 0; col<columns; col++){
                CellState.add(initialState);
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    /**
     * returnerer posisjonen i arrayet som samsvarer med row/col parametrene
     * @param row
     * @param col
     * @return position in array
     */
    private Integer indexOf(int row, int col) throws IndexOutOfBoundsException{
        if (row>rows || col>cols || row<0 || col<0){
            throw new IndexOutOfBoundsException();
        }
        return col+(row)*cols;
    }

    @Override
    public void set(int row, int column, CellState element){
        CellState.set(indexOf(row, column), element);
    }

    @Override
    public CellState get(int row, int column){
        return CellState.get(indexOf(row, column));
    }

    @Override
    public IGrid copy() {
        IGrid newCellGrid = new CellGrid(rows, cols, cellular.CellState.DEAD);
        for (int row = 0; row<rows;row++){
            for (int col = 0; col<cols;col++){
                newCellGrid.set(row, col, this.get(row, col));
            }
        }
        return newCellGrid;
    }
    
}
